package com.lala.thesaboteur;

public class Board {

    public int[] fillLocations(){
        int[] locs = new int[40];
        for(int i=0;i<40;i++){
            if(i==0 || i==2 || i==4){
                locs[i]=3;
            }
            else locs[i]=0;
        }
        return locs;
    }

}
