package com.lala.thesaboteur;

import android.util.Log;
import android.view.View;

public class GameEngine {

    private PathWayCard cardFromHand;
    private ActionCards actionFromHand;

    private boolean skipTurn=false;
    private boolean okay;

    int[] neighbourIndex;

    private  int gameCount=1;
    private boolean deckRanOut=false;
    private int gold=0;

    PathEndCard endCard1,endCard2,endCard3;


    public GameEngine(){
        neighbourIndex = new int[4];
    }

    public boolean canBePlaced(View v, androidx.gridlayout.widget.GridLayout grid, int[] possibleLoc, PathWayCard[] layedCards){

        okay=false;
        int position = grid.indexOfChild(v);
        int i=0;

        neighbourIndex = neighbourCards(position);

        Log.i("canBePlaced", "neighbourIndex: "+neighbourIndex[0]+" "+neighbourIndex[1]+" "+ neighbourIndex[2]+" "+neighbourIndex[3]);

        //checking if the card is connected to any of the neighbours
        while (!okay && i<neighbourIndex.length){
            if(neighbourIndex[i]!=-1 && possibleLoc[neighbourIndex[i]]==2) {
                okay = isConnected(i, neighbourIndex[i], cardFromHand ,layedCards);
            }
            i++;
        }

        //checking the path values of the neighbours and the given card
        i=0;
        while (okay && i<neighbourIndex.length){
            if(neighbourIndex[i]!=-1 && possibleLoc[neighbourIndex[i]]==2) {
                if(!checkPath(i, neighbourIndex[i], cardFromHand ,layedCards)){
                    okay=false;
                }
            }
            i++;
        }
        return okay;
    }



    public int[] neighbourCards(int pos){ //Top,Right,Bottom,Left
        int[] a = new int[4];
        for(int k=0;k<4;k++){
            a[k]=-1;
        }
        ///saves the previous value if the card is on the side of the board
        if((pos-1)>=0  && ((pos-1)%5!=4)){a[3]=pos-1;}
        if((pos+1)<40 && ((pos+1)%5!=0)){a[1]=pos+1;}
        if((pos-5)>=0 ){a[0]=pos-5;}
        if((pos+5)<40 ){a[2]=pos+5;}

        return a;
    }

    public boolean checkPath(int index, int pos, PathWayCard p, PathWayCard[] laidCards){
        Log.i("CheckPath CARD", "id "+p.getId()+ " TOP " + p.getTopSide()+ " RIGHT " + p.getRightSide()+ " BOTTOM " + p.getBottomSide()+ " LEFT " + p.getLeftSide());
        switch (index){
            case 0: //top
                Log.i("CheckPath", ""+ laidCards[pos].getBottomSide()+ " TOP " + p.getTopSide());
                return ((laidCards[pos].getBottomSide()==PathWayCard.cardSide.PATH) && (p.getTopSide()== PathWayCard.cardSide.PATH)) || ((laidCards[pos].getBottomSide()==PathWayCard.cardSide.DEADEND) && (p.getTopSide()== PathWayCard.cardSide.DEADEND));
            case 1: //right
                Log.i("CheckPath", ""+ laidCards[pos].getLeftSide()+ " RIGHT " + p.getRightSide());
                return ((laidCards[pos].getLeftSide()==PathWayCard.cardSide.PATH) && (p.getRightSide()== PathWayCard.cardSide.PATH)) || ((laidCards[pos].getLeftSide()==PathWayCard.cardSide.DEADEND) && (p.getRightSide()== PathWayCard.cardSide.DEADEND));
            case 2://bottom
                Log.i("CheckPath", ""+ laidCards[pos].getTopSide()+ " BOTTOM " + p.getBottomSide());
                return ((laidCards[pos].getTopSide()==PathWayCard.cardSide.PATH) && (p.getBottomSide()== PathWayCard.cardSide.PATH)) || ((laidCards[pos].getTopSide()==PathWayCard.cardSide.DEADEND) && (p.getBottomSide()== PathWayCard.cardSide.DEADEND));
            case 3://left
                Log.i("CheckPath", ""+ laidCards[pos].getRightSide()+ " LEFT " + p.getLeftSide());
                return ((laidCards[pos].getRightSide()==PathWayCard.cardSide.PATH) && (p.getLeftSide()== PathWayCard.cardSide.PATH)) || ((laidCards[pos].getRightSide()==PathWayCard.cardSide.DEADEND) && (p.getLeftSide()== PathWayCard.cardSide.DEADEND));
            default: return true;
        }

    }

    public boolean isConnected(int index, int pos, PathWayCard p, PathWayCard[] layedCars){

        switch (index){
            case 0: //top
                Log.i("CheckPath", ""+layedCars[pos].getBottomSide()+ " TOP " + p.getTopSide());
                return ((layedCars[pos].getBottomSide()==PathWayCard.cardSide.PATH) && (p.getTopSide()== PathWayCard.cardSide.PATH)) ;
            case 1: //right
                Log.i("CheckPath", ""+layedCars[pos].getLeftSide()+ " RIGHT " + p.getRightSide());
                return ((layedCars[pos].getLeftSide()==PathWayCard.cardSide.PATH) && (p.getRightSide()== PathWayCard.cardSide.PATH)) ;
            case 2://bottom
                Log.i("CheckPath", ""+layedCars[pos].getTopSide()+ " BOTTOM " + p.getBottomSide());
                return ((layedCars[pos].getTopSide()==PathWayCard.cardSide.PATH) && (p.getBottomSide()== PathWayCard.cardSide.PATH)) ;
            case 3://left
                Log.i("CheckPath", ""+layedCars[pos].getRightSide()+ " LEFT " + p.getLeftSide());
                return ((layedCars[pos].getRightSide()==PathWayCard.cardSide.PATH) && (p.getLeftSide()== PathWayCard.cardSide.PATH)) ;
            default: return false;
        }

    }


    public boolean lastCardCheck(PathWayCard lastCard, View view, androidx.gridlayout.widget.GridLayout grid){
        switch (gold){
            case 0:
                if(grid.indexOfChild(view)==5) {
                   if(lastCard.getTopSide()==PathWayCard.cardSide.PATH){ return true; }
                   else return false;
                }
                else if( grid.indexOfChild(view)==1){
                    if(lastCard.getLeftSide()==PathWayCard.cardSide.PATH){ return true; }
                    else return false;
                }
                break;
            case 1:
                if(grid.indexOfChild(view)==1){
                    if(lastCard.getRightSide()==PathWayCard.cardSide.PATH){ return true; }
                    else return false;
                }
                else if(grid.indexOfChild(view)==3){
                    if(lastCard.getLeftSide()==PathWayCard.cardSide.PATH){ return true; }
                    else return false;
                }
                else if(grid.indexOfChild(view)==7) {
                    if(lastCard.getTopSide()==PathWayCard.cardSide.PATH){ return true; }
                    else return false;
                }
                break;
            case 2:
                if(grid.indexOfChild(view)==9){
                    if(lastCard.getTopSide()==PathWayCard.cardSide.PATH){ return true; }
                    else return false;
                }
                else if(grid.indexOfChild(view)==3){
                    if(lastCard.getRightSide()==PathWayCard.cardSide.PATH){ return true; }
                    else return false;
                }
                break;
        }
        return false;
        //might use this at the end
        /*
            // 1,5 1,3,7 3,9;
            switch (goldCard){
                case 0:
                    if(myGrid.indexOfChild(view)==1 || myGrid.indexOfChild(view)==5) {
                        socket.emit("gameOver",winner);
                        player.setTurn(false);
                    }
                    break;
                case 1:
                    if(myGrid.indexOfChild(view)==1 || myGrid.indexOfChild(view)==3 || myGrid.indexOfChild(view)==7) {
                        socket.emit("gameOver",winner);
                        player.setTurn(false);
                    }
                    break;
                case 2:
                    if(myGrid.indexOfChild(view)==9 || myGrid.indexOfChild(view)==3){
                        socket.emit("gameOver",winner);
                        player.setTurn(false);
                    }
                    break;
            }*/
    }

    public int flip(View view,androidx.gridlayout.widget.GridLayout myGrid, int[] posLoc, PathWayCard[] laidCards,Deck deck){
        Log.i("FLIP", "flip called!");

        int result =-1;
        if(!getEndCard1().getIsGold()){
            Log.i("getEndCard1()", "flip: 0");
            if(myGrid.indexOfChild(view)==1 || myGrid.indexOfChild(view)==5) {
                result = 0; endCard1.setFlipped(true); posLoc[0]=2; laidCards[0]=(PathWayCard)deck.getDeck()[0];
                if(posLoc[1]!=2){ posLoc[1]=1; }
                if(posLoc[5]!=2){ posLoc[5]=1; }
            }
        }
        if(!getEndCard2().getIsGold()){
            Log.i("getEndCard2()", "not "+getEndCard2().getIsGold());
            if(myGrid.indexOfChild(view)==1 || myGrid.indexOfChild(view)==3 || myGrid.indexOfChild(view)==7) {
                Log.i("TAG", "flip: 1");
                result = 1; endCard2.setFlipped(true); posLoc[2]=2; laidCards[2]=(PathWayCard) deck.getDeck()[0];
                if(posLoc[1]!=2){ posLoc[1]=1; }
                if(posLoc[3]!=2){ posLoc[3]=1; }
                if(posLoc[7]!=2){ posLoc[7]=1; }
            }
        }
        if(!getEndCard3().getIsGold()){
            if(myGrid.indexOfChild(view)==9 || myGrid.indexOfChild(view)==3){
                Log.i("getEndCard3()", "flip: 2");
                result = 2; endCard3.setFlipped(true); posLoc[4]=2; laidCards[4]=(PathWayCard) deck.getDeck()[0];
                if(posLoc[3]!=2){ posLoc[3]=1; }
                if(posLoc[9]!=2){ posLoc[9]=1; }
            }
        }

        return result;
    }

    public boolean checkBlockedGold(int[] pos, PathWayCard[] laidCards){
        //if is the gold and has card around it then it is blocked -> sab won
        boolean result=false;
        switch (gold){
            case 0:
                if(pos[1]==2 && pos[5]==2){
                    if(laidCards[1].getLeftSide() == PathWayCard.cardSide.DEADEND && laidCards[5].getTopSide() == PathWayCard.cardSide.DEADEND){
                        Log.i("TAG", "gold is 0 "+laidCards[1].getLeftSide()+" and "+laidCards[5].getTopSide());

                        result = true;
                    }   break;
                }
            case 1:
                if (pos[1]==2 && pos[3]==2 && pos[7]==2) {
                    if(laidCards[1].getRightSide() == PathWayCard.cardSide.DEADEND && laidCards[7].getTopSide() == PathWayCard.cardSide.DEADEND && laidCards[3].getLeftSide() == PathWayCard.cardSide.DEADEND){
                        result = true;
                        Log.i("TAG", "gold is 0 "+laidCards[1].getLeftSide()+" and "+laidCards[7].getTopSide());
                    }   break;
                }
            case 2:
                if (pos[3]==2 && pos[9]==2) {
                    if(laidCards[3].getRightSide() == PathWayCard.cardSide.DEADEND && laidCards[9].getTopSide() == PathWayCard.cardSide.DEADEND){
                        result = true;
                        Log.i("TAG", "gold is 0 "+laidCards[3].getLeftSide()+" and "+laidCards[9].getTopSide());
                    }   break;
                }
            default: return result;
        }

       return result;
    }



    public void setCardFromHand(PathWayCard p){ this.cardFromHand = p;}
    public PathWayCard getCardFromHand (){ return cardFromHand;}

    public void setActionFromHand(ActionCards p){ this.actionFromHand = p;}
    public ActionCards getActionFromHand (){ return actionFromHand;}

    public int getGameCount() { return gameCount; }
    public void setGameCount(int gameCount) { this.gameCount = gameCount; }

    public boolean getDeckRanOut() { return deckRanOut; }
    public void setDeckRanOut(boolean deckRanOut) { this.deckRanOut = deckRanOut; }

    public int getGold() { return gold; }
    public void setGold(int gold) { this.gold = gold; }

    public PathEndCard getEndCard1() { return endCard1; }
    public void setEndCard1(PathEndCard endCard1) { this.endCard1 = endCard1; }

    public PathEndCard getEndCard2() { return endCard2; }
    public void setEndCard2(PathEndCard endCard2) { this.endCard2 = endCard2; }

    public PathEndCard getEndCard3() { return endCard3; }
    public void setEndCard3(PathEndCard endCard3) { this.endCard3 = endCard3; }

    public boolean getSkipTurn() { return skipTurn; }
    public void setSkipTurn(boolean skipTurn) { this.skipTurn = skipTurn; }
}
