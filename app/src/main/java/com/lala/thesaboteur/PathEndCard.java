package com.lala.thesaboteur;

public class PathEndCard extends Card {

    private boolean isGold;
    private int img;
    private boolean isFlipped = false;
    private boolean isBlocked = false;

    /**
     * Constructor for the abstract Card class
     *
     * @param id the unique identifier for a card
     */
    public PathEndCard(int id, boolean isGold) {
        super(id);
        this.isGold=isGold;
    }

    @Override
    public CARD_TYPE type() {
        return CARD_TYPE.ENDPATH;
    }

    @Override
    public int getImg() {
        return img;
    }

    public boolean getIsGold(){
        return isGold;
    }
    public void setGold(boolean gold) { isGold = gold; }

    public boolean getIsFlipped() { return isFlipped; }
    public void setFlipped(boolean flipped) { isFlipped = flipped; }

    public boolean getIsBlocked() { return isBlocked; }
    public void setBlocked(boolean blocked) { isBlocked = blocked; }
}
