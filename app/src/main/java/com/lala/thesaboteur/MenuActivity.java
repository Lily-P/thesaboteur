package com.lala.thesaboteur;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.io.Serializable;
import java.net.URISyntaxException;

public class MenuActivity extends AppCompatActivity {

    private Button btn, btn2, btn3 ,btn4, btn5;

    public static final String NICKNAME = "usernickname", ROOMNAME="roomname",MULTIPLAYER="multiplayer";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);


        btn = (Button)findViewById(R.id.startButton);
        btn2 = (Button)findViewById(R.id.joinButton);
        btn3 = (Button) findViewById(R.id.singlePlayer);
        btn4 = (Button) findViewById(R.id.quitButton);
        btn5 = (Button) findViewById(R.id.helpButton);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i  = new Intent(MenuActivity.this,NicknameActivity.class);
                i.putExtra(MULTIPLAYER,"create");
                startActivity(i);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i  = new Intent(MenuActivity.this,NicknameActivity.class);
                i.putExtra(MULTIPLAYER,"join");
                startActivity(i);
            }
        });

        //for now
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i  = new Intent(MenuActivity.this,MainActivity.class);
                i.putExtra(NICKNAME,"user");
                i.putExtra(ROOMNAME,"single");
                startActivity(i);
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i  = new Intent(MenuActivity.this,HelpActivity.class);
                startActivity(i);
            }
        });


    }
}
