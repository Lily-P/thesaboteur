package com.lala.thesaboteur;

import android.widget.ImageView;

public class PathWayCard extends Card {

    /** The specific type of the card */
    private PathType type;
    private int img;

    /** The side values of the card */
    private cardSide[] sideValues;
    /**
     * Constructor for the abstract Card class
     *
     * @param id the unique identifier for a card
     */
    public PathWayCard(int id,PathType type, int img) {
        super(id);
        this.type = type;
        this.img = img;
    }

    //all possible cards (up,down,left,right)
    public enum PathType {
        CROSSROAD_PATH(cardSide.PATH, cardSide.PATH, cardSide.PATH, cardSide.PATH),
        VERTICAL_Ta(cardSide.PATH, cardSide.PATH, cardSide.DEADEND, cardSide.PATH),
        VERTICAL_Tb(cardSide.PATH, cardSide.PATH, cardSide.PATH, cardSide.DEADEND),
        HORIZONTAL_Ta(cardSide.DEADEND, cardSide.PATH, cardSide.PATH, cardSide.PATH),
        HORIZONTAL_Tb(cardSide.PATH, cardSide.DEADEND, cardSide.PATH, cardSide.PATH),
        LEFT_RIGHT_PATH(cardSide.DEADEND, cardSide.DEADEND, cardSide.PATH, cardSide.PATH),
        UP_DOWN_PATH(cardSide.PATH, cardSide.PATH, cardSide.DEADEND, cardSide.DEADEND),
        LEFT_UP_TURN(cardSide.PATH, cardSide.DEADEND, cardSide.PATH, cardSide.DEADEND),
        LEFT_DOWN_TURN(cardSide.DEADEND, cardSide.PATH, cardSide.PATH, cardSide.DEADEND),
        RIGHT_UP_TURN(cardSide.PATH, cardSide.DEADEND, cardSide.DEADEND, cardSide.PATH),
        RIGHT_DOWN_TURN(cardSide.DEADEND, cardSide.PATH, cardSide.DEADEND, cardSide.PATH),
        DEADEND_TOP(cardSide.DEADEND, cardSide.PATH, cardSide.DEADEND, cardSide.DEADEND),
        DEADEND_BOTTOM(cardSide.PATH, cardSide.DEADEND, cardSide.DEADEND, cardSide.DEADEND),
        DEADEND_LEFT(cardSide.DEADEND, cardSide.DEADEND, cardSide.DEADEND, cardSide.PATH),
        DEADEND_RIGHT(cardSide.DEADEND, cardSide.DEADEND, cardSide.PATH, cardSide.DEADEND);

        cardSide[] sides;

        PathType(cardSide... sides) {
            this.sides = sides;
        }

    }

    //possible side values of a card
    public enum cardSide {
        DEADEND(0), PATH(1);
        private final int sideVal;

        cardSide(int val) { this.sideVal = val; }

        public int sideVal() { return sideVal; }
    }


    @Override
    public CARD_TYPE type() {
        return CARD_TYPE.PATHWAY;
    }

    public PathType getType(){
        return type;
    }

    public cardSide[] sideValues(){
        sideValues = type.sides.clone();
        return type.sides;
    }

    //[top,bottom,left,right]
    public cardSide getBottomSide(){
        return type.sides[1];
    }
    public cardSide getTopSide(){
        return type.sides[0];
    }
    public cardSide getLeftSide(){
        return type.sides[2];
    }
    public cardSide getRightSide(){
        return type.sides[3];
    }

    @Override
    public int getImg() {return img;}
}
