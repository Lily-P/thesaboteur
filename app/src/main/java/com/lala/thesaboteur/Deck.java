package com.lala.thesaboteur;

import android.util.Log;
import android.widget.ImageView;

public class Deck {

    private final Card[] deck;
    /** images array */
    int[] myImageList = new int[]{R.drawable.cross_road,R.drawable.vertical_t_right, R.drawable.vertical_t_left,  R.drawable.horizontal_t_down,R.drawable.horizontal_t_up,
    R.drawable.left_right_path,R.drawable.up_down_path, R.drawable.left_up_turn, R.drawable.left_down_turn,
    R.drawable.right_up_turn,R.drawable.right_down_turn,R.drawable.deadend_top,R.drawable.deadend_bottom,R.drawable.deadend_left,R.drawable.deadend_right,
    R.drawable.trolley_repair, R.drawable.broken_trolley, R.drawable.lamp_repair,R.drawable.broken_lamp, R.drawable.prick_repair, R.drawable.broken_prick, R.drawable.map};

    public Deck(){
        deck = new Card[64];

        int i=0,j=0;
        for(PathWayCard.PathType card: PathWayCard.PathType.values()){
            deck[i] = new PathWayCard(i, card, myImageList[j]);
            deck[i+1] = new PathWayCard(i+1, card, myImageList[j]);
            if(j<14) {
                deck[i+2] = new PathWayCard(i+2, card, myImageList[j]);
                i+=3;
            }
            else{
                i+=2;
            }
            j++;
        }
        for(ActionCards.ActionCardType card: ActionCards.ActionCardType.values()){
            deck[i] = new ActionCards(i, card, myImageList[j]);
            deck[i + 1] = new ActionCards(i+1, card, myImageList[j]); //
            if(j<21) {
                deck[i+2] = new ActionCards(i+2, card, myImageList[j]);
                i+=3;
            }
            else{
                i+=2;
            }
            j++;
        }

    }

    public void printDeck(){
        for(int i=0;i<deck.length;i++){
            if(deck[i] instanceof PathWayCard){
                Log.i("deck", "card type "+((PathWayCard)deck[i]).getType()+" "+((PathWayCard)deck[i]).getId() +" SIDES: "+ ((PathWayCard)deck[i]).getTopSide() + " "+ ((PathWayCard)deck[i]).getBottomSide() + " "+((PathWayCard)deck[i]).getLeftSide() + " "+((PathWayCard)deck[i]).getRightSide() );
            }
            else {
                Log.i("deck", "card type "+((ActionCards)deck[i]).getType()+" "+((ActionCards)deck[i]).getId());
            }
        }
    }

    /** returns image resource file
     * @param  j is the id of a card in the deck
     *
     * */
    public int getImageRes(int j){
        return deck[j].getImg();
    }

    /** returns specific card from the deck
     * @param a is the Id of the card that we want to fetch
     */
    public Card getCardAt(int a){
        for (int i=0;i<deck.length;i++){
            if(a==deck[i].getId()){
                return deck[i];
            }
        }
        return null;
    }

    public Card[] getDeck(){
        return deck;
    }
}
