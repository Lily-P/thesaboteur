package com.lala.thesaboteur;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    //imageViews
    ImageView card1,card2,card3,card4,card5,card6,usericon1,usericon2,usericon3,start,gold1,gold2,gold3;
    ImageView actionIcon1,actionIcon2,actionIcon3,myActionIcon;

    TextView textRole,user1,user2,user3,round,myPoints,points1,points2,points3,skipTurn;
    ProgressBar load;
    Deck deck;
    Board board = new Board();
    GameEngine gameEngine;
    int idOfGoldCard, goldCard;
    int deletedCardId=-1;
    int idOfMovedCard=0;

    //declare socket
    private Socket socket;
    private String Nickname, roomName;
    private String id;

    //declaration of player(s)
    private Player player;
    HashMap<String,Player> connectedPlayers;

    int[] possibleLocations = new int[40];
    PathWayCard[] laidCards = new PathWayCard[40];
    androidx.gridlayout.widget.GridLayout myGrid;
    private SinglePlayerHandler singlePlayerHandler;

    public void cardClick(View view){
        if(player.getTurn()) {
            switch (view.getId()) {
                case R.id.card1:
                    deletedCardId = 0;
                    break;
                case R.id.card2:
                    deletedCardId = 1;
                    break;
                case R.id.card3:
                    deletedCardId = 2;
                    break;
                case R.id.card4:
                    deletedCardId = 3;
                    break;
                case R.id.card5:
                    deletedCardId = 4;
                    break;
                case R.id.card6:
                    deletedCardId = 5;
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + view.getId());
            }
            view.setAlpha(0);

            //check if the clicked card is an action card and enable its effects
            if(!gameEngine.getSkipTurn()){
                if (player.getHand().get(deletedCardId) instanceof ActionCards) {
                    //check if peek
                    if (((ActionCards) player.getHand().get(deletedCardId)).getType().equals(ActionCards.ActionCardType.MAP)) {
                        player.setPeekStatus(true);
                    }
                    gameEngine.setActionFromHand((ActionCards) player.getHand().get(deletedCardId));
                }
                else if(player.getHand().get(deletedCardId) instanceof PathWayCard){
                    gameEngine.setCardFromHand((PathWayCard) player.getHand().get(deletedCardId));
                    ///highlight the possible locations
                    ImageView img;
                    for (int i = 0; i < possibleLocations.length; i++) {
                        if (possibleLocations[i] == 1) {
                            img = (ImageView) myGrid.getChildAt(i);
                            img.setImageResource(R.drawable.mycell2);
                            img.setAlpha(1f);
                        }
                    }
                }
            }
            else {
                socket.emit("changeTurn");
                player.setTurn(false);
                gameEngine.setSkipTurn(false);
            }

            idOfMovedCard = player.getHand().get(deletedCardId).getId();
            Log.i("idforsend", "card id " + idOfGoldCard);
            player.getHand().remove(deletedCardId);

            if (player.getHand().size() < 6) {
                socket.emit("requestNewCard");
            }
            Log.i("deleted", "card no " + deletedCardId + " size of hand " + player.getHand().size());
        }
        else
            customToast("Not your turn!",1);

    }

    public void moveCard(View view){
        Log.i("moveCard method","there clicked");
            ImageView img;
            for(int i=0;i<possibleLocations.length;i++){
                if(possibleLocations[i]==1){
                    img = (ImageView) myGrid.getChildAt(i);
                    img.setAlpha(0f);
                }
            }

        if(player.getTurn() && player.getMovable()){

            //check if location is available, make chosen place immutable and set image
            if(possibleLocations[myGrid.indexOfChild(view)]==1){
                if(gameEngine.canBePlaced(view,myGrid,possibleLocations, laidCards)){
                    possibleLocations[myGrid.indexOfChild(view)]=2;
                    addAvailableLocations((ImageView)view);

                    //
                    laidCards[myGrid.indexOfChild(view)] = gameEngine.getCardFromHand();

                    //set image to proper resource
                    Drawable res = getResources().getDrawable(gameEngine.getCardFromHand().getImg());
                    ((ImageView)view).setImageDrawable(res);
                    view.setAlpha(1f);
                }
                else {
                    customToast("Card can not be placed",1);
                }
            }

            //check if the location is for win
                determineWinner(view,"");

            //create json obj for sending
            JSONObject movedCardData = new JSONObject();
            JSONArray myarr = new JSONArray();

            try {
                for (int possibleLocation : possibleLocations) {
                    myarr.put(possibleLocation);
                }

                movedCardData.put("movedCardId",gameEngine.getCardFromHand().getId());
                movedCardData.put("movedPlaceId",view.getId());
                movedCardData.put("movedPlaceGrid",myGrid.indexOfChild(view));
                movedCardData.put("updatedPositions",myarr);

                Log.i("movedCardId"," "+gameEngine.getCardFromHand().getId());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            //check if the gold can not be reached anymore
            if(gameEngine.checkBlockedGold(possibleLocations, laidCards)){
                Log.i("checkBlockedGold"," IN ");

                JSONObject winner = new JSONObject();
                try { winner.put("winner", "The saboteur");
                    winner.put("role", 1);
                    winner.put("id", "");}
                catch (JSONException e) { e.printStackTrace(); }

                socket.emit("gameOver", winner);
                player.setTurn(false);
            }

            //send the moved card location and id
            socket.emit("cardMoved", movedCardData);
            socket.emit("changeTurn");
            player.setTurn(false);
            Log.i("Turn", ""+player.getTurn());
        }
        else{
            if(!player.getTurn()) customToast("Not your turn",1);
            else if(player.getTurn() && !player.getMovable()){
                customToast("You are blocked!",1);
                socket.emit("changeTurn");
            }
        }

    }

    public void peek(View view){
        Log.i("peek","method called");
        Log.i("ids",""+view.getId()+" "+ idOfGoldCard);
        if(player.getPeekStatus()){
            if(view.getId() == idOfGoldCard){
                customToast("This is the gold card",0);
            }
            else {
                customToast("This is no the gold card",0);
            }
            player.setPeekStatus(false);
        }
    }

    public void actionClick(View view){
        //check what was the action card
        //check the user
        //emit event about used action card
        ActionCards c = gameEngine.getActionFromHand();

        for(Player p: connectedPlayers.values()){
            if(p.getName().equals(((TextView)view).getText().toString())){

                JSONObject data = new JSONObject();
                try {
                    data.put("usedCard",c.getType().toString());
                    data.put("usedAgainst",p.getID());
                    Log.i("actionClick"," type of used action card "+c.getType().toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                socket.emit("actionUsed",data);
                socket.emit("changeTurn");
            }
        }
    }

    public void actionClickMe(View view){
        //check the used(clicked) card
        //check the card that is blocking ME
        //send everyone USED CARD

        //this is a card that I saved from hand click
        ActionCards c = gameEngine.getActionFromHand();

        switch (c.getType()){
            case LAMP_REPAIR:
                player.setBrokenLamp(false); break;
            case TROLLEY_REPAIR:
                player.setBrokenTrolley(false); break;
            case PICK_REPAIR:
                player.setBrokenPick(false); break;
            default: throw new IllegalStateException("Unexpected value");
        }

        if(!player.getLampStatus() || !player.getPickStatus() || !player.getTrolleyStatus()) {
            player.setMovable(true);
        }
        else player.setMovable(false);

        displayAttack(player.getID(),"");

        JSONObject data = new JSONObject();
        try {
            data.put("usedCard",c.getType().toString());
            data.put("usedAgainst",player.getID());

            Log.i("actionClickME"," type of used action card "+c.getType().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        socket.emit("actionUsed",data);
        socket.emit("changeTurn");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Nickname = getIntent().getExtras().getString(NicknameActivity.NICKNAME);
        roomName = getIntent().getExtras().getString(NicknameActivity.ROOMNAME);
        connectedPlayers = new HashMap<>();

        Log.i("RoomName","room name "+roomName);

        gameEngine = new GameEngine();
        singlePlayerHandler = new SinglePlayerHandler();

        //connecting to socket
        connectToServer();

        //send alert about single mode
        if(roomName.equals("single")){socket.emit("single",Nickname);}
        else socket.emit("join",Nickname,roomName);

        //testing deck
        deck = new Deck();
        deck.printDeck();

        //add starting card to possible locations
        start = findViewById(R.id.start);
        myGrid = findViewById(R.id.gridLayout);

        //adding starting card to laid  card
        laidCards[myGrid.indexOfChild(start)] = new PathWayCard(((PathWayCard)deck.getDeck()[0]).getId(),((PathWayCard)deck.getDeck()[0]).getType(),((PathWayCard)deck.getDeck()[0]).getImg());
        for(int i = 0; i< laidCards.length; i++){
            Log.i("THe laid cards", "o: "+ laidCards[i]);
        }

        //fill the array with 0
        possibleLocations = board.fillLocations();
        possibleLocations[myGrid.indexOfChild(start)]=2;
        addAvailableLocations(start);

        configSocketEvents();

        //set round count
        round = (TextView)findViewById(R.id.round);
        round.setText("1");

        gold1 = findViewById(R.id.imageView1);
        gold2 = findViewById(R.id.imageView3);
        gold3 = findViewById(R.id.imageView5);

        card1 = findViewById(R.id.card1);
        card2 = findViewById(R.id.card2);
        card3 = findViewById(R.id.card3);
        card4 = findViewById(R.id.card4);
        card5 = findViewById(R.id.card5);
        card6 = findViewById(R.id.card6);

        skipTurn = findViewById(R.id.discard);

        load = findViewById(R.id.progressBar3);
    }

    public void skipTurn(View view){
        Log.i("skipTurn", "called ");
        customToast("Choose card from hand to discard",0);
        gameEngine.setSkipTurn(true);
    }

    public void homePage(View view){
        Intent in  = new Intent(MainActivity.this,MenuActivity.class);
        startActivity(in);
        finish();
    }


    public void connectToServer(){
        try {
            socket = IO.socket("http://10.0.2.2:3000");
            socket.connect();
            Log.i("connectToServer", "connected ");

        } catch (URISyntaxException e) {
            e.printStackTrace();
            Log.d("fail", "Failed to connect"); }
    }

    public void configSocketEvents(){
        Log.i("configSocketEvents", "called ");

        socket.on("joinedRoom", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                final TextView roomName = findViewById(R.id.textViewforRoom);
                String r;
                try {
                    r = data.getString("r");
                    Log.i("Room", "joined "+r);

                    final String finalR=r;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            roomName.setText(finalR);
                            roomName.setVisibility(View.VISIBLE);
                        }
                    });

                } catch (JSONException e) {
                    Log.i("SocketIO", "Error getting ID");
                }
            }
        }).on("socketID", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                try {
                    id = data.getString("id");

                    player = new Player(id,Nickname);
                    Log.i("SocketIO", "my ID: " + id);

                } catch (JSONException e) {
                    Log.i("SocketIO", "Error getting ID");
                }
            }
        }).on("newPlayer", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject)args[0];
                try {
                    id = data.getString("id");
                    String nameExtra = data.getString("username");
                    connectedPlayers.put(id,new Player(id,nameExtra));
                    Log.i("SocketIO", "new player's id: " + id + " "+nameExtra);

                } catch (JSONException e) {
                    Log.i("error from new player", e.getMessage());
                    Log.i("SocketIO,Error", "Error getting new player's ID");
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() { displayPlayers(); }
                });

            }
        }).on("playerDisconnected", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    JSONObject data;
                    try {
                        data = new JSONObject(String.valueOf(args[0]));
                        id = data.getString("id");
                        customToast(connectedPlayers.get(id).getName()+" has left the game",0);

                        if((!connectedPlayers.isEmpty())){
                            connectedPlayers.remove(id);
                        }
                    } catch (JSONException e) {
                        Log.i("playerDisconnected", e.getMessage());
                    }
                    }
                });
            }
        }).on("gameOverGuys", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                JSONObject data;

                singlePlayerHandler.setBlockMoves(true);
                myPoints = findViewById(R.id.mypoints);
                try {
                    data = new JSONObject(String.valueOf(args[0]));
                    final String winner = data.getString("winner");
                    JSONArray points = data.getJSONArray("points");
                    JSONArray ids = data.getJSONArray("ids");

                    for(int i=0;i<ids.length();i++){
                        if(ids.get(i).toString().equals(player.getID())){
                            player.setPoints(points.getInt(i));
                        }
                        else {
                            if(connectedPlayers.containsKey(ids.get(i)))
                                connectedPlayers.get(ids.get(i)).setPoints(points.getInt(i));
                        }
                    }

                    runOnUiThread(new Runnable() {@Override public void run() {
                        renderMessage(winner);
                        myPoints.setText(String.valueOf(player.getPoints()));
                        displayPoints();
                    }});

                    player.setTurn(false);

                } catch (JSONException e) { Log.i("gameOverGuys,Error", e.getMessage()); }
            }
        }).on("getPlayers", new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                JSONArray playersList = (JSONArray) args[0];
                try {
                    int len=playersList.length();
                    String friendID ;
                    String friendName;

                    for (int i = 0; i<len;i++){
                        friendID = playersList.getJSONObject(i).getString("id");
                        friendName = playersList.getJSONObject(i).getString("username");
                        Player friend = new Player(friendID,friendName);
                        Log.i("SocketIO,getPlayers", "id from array " + playersList.getJSONObject(i).getString("username"));

                        if(!friend.getID().equals(player.getID())){
                            connectedPlayers.put(playersList.getJSONObject(i).getString("id"),friend);
                        }
                    }

                } catch (JSONException e) {
                    Log.i( "SocketIO,Error", "Error getting player's ID from array");
                    Log.i("Error",e.getMessage());
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() { displayPlayers(); }});
            }
        }).on("dealCards", new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                JSONObject data = (JSONObject)args[0];
                try {
                    JSONArray hand = data.getJSONArray("hand");
                    int len = hand.length();

                    //get cards from server
                    Log.i("dealCards event", "here comes dealt cards from server");
                    for(int i=0;i < len;i++){
                        int cardNo = hand.getInt(i);
                        Log.i("hand","cards no: "+cardNo);
                        player.getHand().add(deck.getDeck()[cardNo]);
                    }

                    //add corresponding cards from deck to players hand
                    for(int i=0;i<player.geSizeOfHand();i++){
                        if(player.getHand().get(i) instanceof PathWayCard) {
                            Log.i("hand", "my cards: " + ((PathWayCard) player.getHand().get(i)).getType() + " " + player.getHand().get(i).getId());
                        }
                        else {
                            Log.i("hand", "my cards: " + ((ActionCards) player.getHand().get(i)).getType() + " " + player.getHand().get(i).getId());
                        }
                    }

                    int role = data.getInt("role");
                    player.setRole(role);
                    Log.i("role","my role is: "+player.getRole());

                    textRole = findViewById(R.id.role);
                    if(role==1) textRole.setText("Saboteour");
                    else textRole.setText("Gold Miner");

                    //get which cards is gold
                    goldCard = data.getInt("gold");
                    gameEngine.setGold(data.getInt("gold"));
                    Log.i("gold card","gold card is: "+ goldCard);
                    idOfGoldCard = setGoldCard(goldCard);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() { displayDealtCards();
                        }
                    });

                } catch (JSONException e) {
                    Log.i("DealCards,Error", e.getMessage());
                }
            }
        }).on("refill", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data;
                int myRefillCard;

                try {
                    data = new JSONObject(String.valueOf(args[0]));
                    myRefillCard = data.getInt("refillCard");

                    if(deletedCardId!=-1 && myRefillCard!=-1) {
                        player.getHand().add(deletedCardId, deck.getDeck()[myRefillCard]);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                displayNewCards();
                            }
                        });
                    }
                    else if(myRefillCard==-1){
                        player.getHand().add(deletedCardId, new PathEndCard(9,false));
                    }
                } catch (JSONException e) {
                    Log.i("SocketIO,Error", e.getMessage());
                }
            }
        }).on("playersMove", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data;

                int movedPlace = 0,movedCard = 0,movedPlaceOnGrid=0;
                try {
                    data = new JSONObject(String.valueOf(args[0]));

                    movedCard = data.getInt("movedCard");
                    movedPlace = data.getInt("movedPlace");
                    movedPlaceOnGrid = data.getInt("movedPlaceGrid");

                    //updated possible locations
                    JSONArray pLocations  = data.getJSONArray("possibleLocations");
                    for(int a=0;a<pLocations.length();a++){
                        possibleLocations[a]=pLocations.getInt(a);
                    }

                    if(deck.getCardAt(movedCard) instanceof PathWayCard){
                        laidCards[movedPlaceOnGrid] = (PathWayCard)deck.getCardAt(movedCard);
                    }
                    Log.i("moved card ",movedCard+" moved place id "+movedPlace);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //getting the source of img and setting it to appropriate view
                final int myResource = deck.getImageRes(movedCard);
                final ImageView moved = findViewById(movedPlace);
                final int ind = myGrid.indexOfChild(findViewById(movedPlace));

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    //to make sure not to display unwanted stuff
                    if(possibleLocations[ind]==2){
                        moved.setImageResource(myResource);
                        moved.setAlpha(1f);
                    }
                    }
                });
            }
        }).on("turnOf", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                String iD = null;

                JSONObject data;
                try {
                    data = new JSONObject(String.valueOf(args[0]));
                    iD = data.getString("turn");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(iD.equals(player.getID())){
                    player.setTurn(true);
                    if(!checkEmptyHand()){
                        Log.i("Turn of event","turn: "+player.getTurn());
                        runOnUiThread(new Runnable() {@Override public void run() { customToast("Your turn",0); }});
                    }
                }
                else {
                    if(connectedPlayers.containsKey(iD)){
                        connectedPlayers.get(iD).setTurn(true);
                    }
                }
            }
        }).on("attacked", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data;

                //cases when attacked is me and someone else
                try {
                    data = new JSONObject(String.valueOf(args[0]));
                    final String attackingCard = data.getString("usedCard");
                    final String idOfAttacked = data.getString("usedAgainst");

                    Log.i("attacked", "the card used "+attackingCard+" the id of attacked " +idOfAttacked);

                    if(idOfAttacked.equals(player.getID())){
                        enableActionCardEffects(attackingCard);
                    }
                    else {setAttack(idOfAttacked,attackingCard);}
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() { displayAttack(idOfAttacked,attackingCard);
                        }

                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }).on("opponentMove", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i("opponentMove", "WORKS!");
                String sm="";
                JSONObject data;

                //so here I need a method to trigger proper opponents make a move
                try {
                    data = new JSONObject(String.valueOf(args[0]));
                    sm = data.getString("turn");
                    Log.i("opponentMove", " "+sm);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                boolean stop=false;
                int j=0;
                //the the first card from hand // use alg to see if it can be laid or played // display the move // refill hand // send the request back
                Collections.shuffle(connectedPlayers.get(sm).getHand());

                Log.i("BLOCkED MOVES", " "+singlePlayerHandler.getBlockMoves());

                if(!singlePlayerHandler.getBlockMoves()) {
                    while (!stop && j < connectedPlayers.get(sm).getHand().size()) {
                        if (connectedPlayers.get(sm).getHand().get(j) instanceof PathWayCard) {
                            final ArrayList<Integer> loc = singlePlayerHandler.moveHandler(possibleLocations);

                            gameEngine.setCardFromHand((PathWayCard) connectedPlayers.get(sm).getHand().get(j));

                            final String finalSm = sm;
                            boolean foundLoc = false;
                            int l = 0;

                            while ((!foundLoc) && l < loc.size()) {
                                Log.i("ARGUMENTS", "View at " + loc.get(l));
                                if (gameEngine.canBePlaced(myGrid.getChildAt(loc.get(l)), myGrid, possibleLocations, laidCards)) {
                                    possibleLocations[loc.get(l)] = 2;
                                    addAvailableLocations((ImageView) myGrid.getChildAt(loc.get(l)));

                                    //check winner
                                    determineWinner(myGrid.getChildAt(loc.get(l)), sm);

                                    laidCards[loc.get(l)] = (PathWayCard) connectedPlayers.get(finalSm).getHand().get(j);

                                    //set image to proper resource
                                    final int finalL = l;
                                    final int finalJ = j;

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Drawable res = getResources().getDrawable(connectedPlayers.get(finalSm).getHand().get(finalJ).getImg());
                                            ((ImageView) myGrid.getChildAt(loc.get(finalL))).setImageDrawable(res);
                                            myGrid.getChildAt(loc.get(finalL)).animate().alpha(1f).setDuration(2000);
                                        }
                                    });

                                    if(gameEngine.checkBlockedGold(possibleLocations, laidCards)){
                                        Log.i("checkBlockedGold"," single-player ");

                                        JSONObject winner = new JSONObject();
                                        try { winner.put("winner", "The saboteur");
                                            winner.put("role", 1);
                                            winner.put("id", "");
                                        }
                                        catch (JSONException e) { e.printStackTrace(); }

                                        socket.emit("gameOver", winner);
                                    }

                                    foundLoc = true;
                                    stop = true;
                                } else {
                                    Log.i("NOT A ", "PROPER CARD ");
                                    l++;
                                }
                            }
                        } else Log.i("opponentMove", "its an action card");
                        j++;
                    }
                }

            }
        }).on("opponentsHand", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i("opponentsHand", "called!");
                JSONObject data;

                try {
                    data = new JSONObject(String.valueOf(args[0]));

                    singlePlayerHandler.initializeHands(connectedPlayers, data.getJSONArray("opHand"), data.getJSONArray("opId"), deck);
                    JSONArray roles = data.getJSONArray("opRole");

                    for(int i=0; i< connectedPlayers.values().size();i++){
                        connectedPlayers.values().iterator().next().setRole(roles.getInt(i));
                        Log.i("example roles and hand", " "+connectedPlayers.values().iterator().next().getRole()+" "+connectedPlayers.values().iterator().next().getHand().get(0).getId());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("ERROR opphand", e.getMessage());
                }
            }
        }).on("deckRanOut", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i("deckRanOut", "called ");
                gameEngine.setDeckRanOut(true);
            }
        }).on("flippedCard", new Emitter.Listener(){
            @Override public void call(Object... args) {
                Log.i("flippedCard", "called ");

                JSONObject data;

                try {
                    data = new JSONObject(String.valueOf(args[0]));
                    int flippedCard = data.getInt("flipped");
                    Log.i("flippedCard", "data "+flippedCard);

                    final int i = flippedCard;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() { displayFlipFromSoc(i); }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }).on("waitingForPlayers", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                //////////
                Log.i("WAIT", "room size is less than needed ");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() { load.setAlpha(1f); }
                });
            }
        }).on("canPlay", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i("START", "room size is OK ");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() { load.setAlpha(0f); }
                });
            }
        });
    }

        public void displayDealtCards(){

            card1.setImageResource((player.getHand().get(0)).getImg());
            card2.setImageResource((player.getHand().get(1)).getImg());
            card3.setImageResource((player.getHand().get(2)).getImg());
            card4.setImageResource((player.getHand().get(3)).getImg());
            card5.setImageResource((player.getHand().get(4)).getImg());
            card6.setImageResource((player.getHand().get(5)).getImg());

            card1.setVisibility(View.VISIBLE);
            card2.setVisibility(View.VISIBLE);
            card3.setVisibility(View.VISIBLE);
            card4.setVisibility(View.VISIBLE);
            card5.setVisibility(View.VISIBLE);
            card6.setVisibility(View.VISIBLE);

        }

        //////////not working yet
        public void displayTurns(){
            if(player.getTurn() && player.getMovable()){
               // textRole = findViewById(R.id.role);
               // textRole.setTextColor(0xffffbb33);

               // customToast("Your turn",0);
            }
            else {
                for(Player p: connectedPlayers.values()){
                    if(p.getTurn()){
                        if(user1.getText().toString()==p.getName()){user1.setTextColor(0xffffbb33); }
                        else if(user2.getText().toString()==p.getName()){user2.setTextColor(0xffffbb33);}
                        else if(user3.getText().toString()==p.getName()){user3.setTextColor(0xffffbb33);}
                    }
                }
            }
            //reset the colors to original
        }

        public void displayFlip(View view){

            int flipCardID = gameEngine.flip(view,myGrid,possibleLocations, laidCards,deck);

            switch (flipCardID){
                case 0: gold1.setImageResource(R.drawable.cross_road); gold1.setRotation(90); gold1.setScaleX(1.4f); gold1.setScaleY(1.4f); break;
                case 1: gold2.setImageResource(R.drawable.cross_road); gold2.setRotation(90); gold2.setScaleX(1.4f); gold2.setScaleY(1.4f); break;
                case 2: gold3.setImageResource(R.drawable.cross_road); gold3.setRotation(90); gold3.setScaleX(1.4f); gold3.setScaleY(1.4f);break;
                case -1: break;
                default:
                    throw new IllegalStateException("Unexpected value");
            }
            JSONObject idOfFlipped = new JSONObject();
            try { idOfFlipped.put("flipped",flipCardID); }
            catch (JSONException e) { e.printStackTrace(); }

            socket.emit("goldFlipped",idOfFlipped);

        }

        public void displayFlipFromSoc(int i){
        switch (i){
            case 0: gameEngine.getEndCard1().setFlipped(true); gold1.setImageResource(R.drawable.cross_road); gold1.setRotation(90); gold1.setScaleX(1.4f); gold1.setScaleY(1.4f);
                possibleLocations[0]=2; laidCards[0]=(PathWayCard)deck.getDeck()[0];
                if(possibleLocations[1]!=2){ possibleLocations[1]=1; }
                if(possibleLocations[5]!=2){ possibleLocations[5]=1; }
                break;
            case 1: gameEngine.getEndCard2().setFlipped(true); gold2.setImageResource(R.drawable.cross_road); gold2.setRotation(90); gold2.setScaleX(1.4f); gold2.setScaleY(1.4f);
                possibleLocations[2]=2; laidCards[2]=(PathWayCard) deck.getDeck()[0];
                if(possibleLocations[1]!=2){ possibleLocations[1]=1; }
                if(possibleLocations[3]!=2){ possibleLocations[3]=1; }
                if(possibleLocations[7]!=2){ possibleLocations[7]=1; }
                break;
            case 2: gameEngine.getEndCard3().setFlipped(true); gold3.setImageResource(R.drawable.cross_road); gold3.setRotation(90); gold3.setScaleX(1.4f); gold3.setScaleY(1.4f);
                possibleLocations[4]=2; laidCards[4]=(PathWayCard) deck.getDeck()[0];
                if(possibleLocations[3]!=2){ possibleLocations[3]=1; }
                if(possibleLocations[9]!=2){ possibleLocations[9]=1; }
                break;
            case -1: break;
            default:
                throw new IllegalStateException("Unexpected value");
        }
    }

        public void displayNewCards(){
            switch (deletedCardId){
                case 0:
                    card1.setImageResource((player.getHand().get(0)).getImg());
                    card1.animate().alpha(1f).setDuration(2000);
                    break;
                case 1:
                    card2.setImageResource((player.getHand().get(1)).getImg());
                    card2.animate().alpha(1f).setDuration(2000);
                    break;
                case 2:
                    card3.setImageResource((player.getHand().get(2)).getImg());
                    card3.animate().alpha(1f).setDuration(2000);
                    break;
                case 3:
                    card4.setImageResource((player.getHand().get(3)).getImg());
                    card4.animate().alpha(1f).setDuration(2000);
                    break;
                case 4:
                    card5.setImageResource((player.getHand().get(4)).getImg());
                    card5.animate().alpha(1f).setDuration(2000);
                    break;
                case 5:
                    card6.setImageResource((player.getHand().get(5)).getImg());
                    card6.animate().alpha(1f).setDuration(2000);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value");
            }
        }

        public void displayPlayers(){

            user1 = findViewById(R.id.user1);
            user2 = findViewById(R.id.user2);
            user3 = findViewById(R.id.user3);

            usericon1 = findViewById(R.id.user1icon);
            usericon2 = findViewById(R.id.user2icon);
            usericon3 = findViewById(R.id.user3icon);

            points1 = findViewById(R.id.points1);
            points2 = findViewById(R.id.points2);
            points3 = findViewById(R.id.points3);

            ArrayList<String> listOfKeys = new ArrayList<>(connectedPlayers.keySet());
            ArrayList<Player> listOfValues = new ArrayList<>(connectedPlayers.values());


            if(!connectedPlayers.isEmpty()){
                switch (connectedPlayers.size()){
                    case 1:
                        user1.setText(listOfValues.get(0).getName());
                        usericon1.setVisibility(View.VISIBLE);
                        user1.setVisibility(View.VISIBLE);
                        points1.setAlpha(1f);
                        break;

                    case 2:
                        user1.setText(listOfValues.get(0).getName());
                        user2.setText(listOfValues.get(1).getName());

                        usericon1.setVisibility(View.VISIBLE);
                        usericon2.setVisibility(View.VISIBLE);

                        user1.setVisibility(View.VISIBLE);
                        user2.setVisibility(View.VISIBLE);

                        points1.setAlpha(1f);
                        points2.setAlpha(1f);
                        break;
                    case 3:
                        user1.setText(listOfValues.get(0).getName());
                        user2.setText(listOfValues.get(1).getName());
                        user3.setText(listOfValues.get(2).getName());

                        usericon1.setVisibility(View.VISIBLE);
                        usericon2.setVisibility(View.VISIBLE);
                        usericon3.setVisibility(View.VISIBLE);

                        user1.setVisibility(View.VISIBLE);
                        user2.setVisibility(View.VISIBLE);
                        user3.setVisibility(View.VISIBLE);

                        points1.setAlpha(1f);
                        points2.setAlpha(1f);
                        points3.setAlpha(1f);
                        break;
                }
            }
        }

        public void displayAttack(String id, String card){
            Log.i("displayAttack", "method");

            int resource=0;
            if(!card.isEmpty()) {
                switch (card) {
                    case "BROKEN_PICK": resource = R.drawable.pick; break;
                    case "BROKEN_TROLLEY": resource = R.drawable.trolley; break;
                    case "BROKEN_LAMP": resource = R.drawable.lamp; break;
                }
            }

            //block1,block2,block
            actionIcon1 = findViewById(R.id.block1);
            actionIcon2 = findViewById(R.id.block2);
            actionIcon3 = findViewById(R.id.block);
            myActionIcon = findViewById(R.id.block3);

            Player p;

            if(id.equals(player.getID())){
                if(!player.getMovable()){
                    myActionIcon.setImageResource(resource); myActionIcon.setAlpha(1f);
                }
                else {
                    myActionIcon.setAlpha(0f);
                }
            }
            else {
                p = fetchPlayerWithId(id);

                if (p.getName().equals(user1.getText().toString()) && !p.getMovable()){
                    actionIcon1.setImageResource(resource);  actionIcon1.setAlpha(1f);
                }
                else if(p.getName().equals(user1.getText().toString())&& p.getMovable()) { actionIcon1.setAlpha(0f); }

                if(p.getName().equals(user2.getText().toString()) && !p.getMovable()){
                    actionIcon2.setImageResource(resource); actionIcon2.setAlpha(1f);
                }
                else if(p.getName().equals(user2.getText().toString()) && p.getMovable()) { actionIcon2.setAlpha(0f); }

                if(p.getName().equals(user3.getText().toString()) && !p.getMovable()){
                    actionIcon3.setImageResource(resource); actionIcon3.setAlpha(1f);
                }
                else if(p.getName().equals(user3.getText().toString()) && p.getMovable()) { actionIcon3.setAlpha(0f); }

            }

        }

        public void displayPoints(){
            points1 = findViewById(R.id.points1);
            points2 = findViewById(R.id.points2);
            points3 = findViewById(R.id.points3);

            user1 = findViewById(R.id.user1);
            user2 = findViewById(R.id.user2);
            user3 = findViewById(R.id.user3);

            ArrayList<String> listOfKeys = new ArrayList<>(connectedPlayers.keySet());
            ArrayList<Player> listOfValues = new ArrayList<>(connectedPlayers.values());

            for (int i=0;i<listOfKeys.size();i++){
                switch (i){
                    case 0:
                        points1.setText(String.valueOf(listOfValues.get(i).getPoints()));
                        break;
                    case 1:
                        points2.setText(String.valueOf(listOfValues.get(i).getPoints()));
                        break;
                    case 2:
                        points3.setText(String.valueOf(listOfValues.get(i).getPoints()));
                        break;
                }

            }
        }

        public void addAvailableLocations(ImageView view){
             int i = myGrid.indexOfChild(view);
                if(possibleLocations[i]==2){
                    if((i-1)>=0 && possibleLocations[i-1]==0 && ((i-1)%5!=4)){possibleLocations[i-1]=1;}
                    if((i+1)<40 && possibleLocations[i+1]==0 && ((i+1)%5!=0)){possibleLocations[i+1]=1;}
                    if((i-5)>=0 && possibleLocations[i-5]==0){possibleLocations[i-5]=1;}
                    if((i+5)<40 && possibleLocations[i+5]==0){possibleLocations[i+5]=1;}
                }

         }

        public int setGoldCard(int num){
            int idOfGoldCardImage=0;

             gameEngine.setEndCard1(new PathEndCard(1,true));
            switch (num){
                case 0:
                    gameEngine.setEndCard1(new PathEndCard(1,true));
                    gameEngine.setEndCard2(new PathEndCard(1,false));
                    gameEngine.setEndCard3(new PathEndCard(1,false));
                    idOfGoldCardImage=gold1.getId();break;
                case 1:
                    gameEngine.setEndCard1(new PathEndCard(1,false));
                    gameEngine.setEndCard2(new PathEndCard(1,true));
                    gameEngine.setEndCard3(new PathEndCard(1,false));
                    idOfGoldCardImage=gold2.getId(); break;
                case 2:
                    gameEngine.setEndCard1(new PathEndCard(1,false));
                    gameEngine.setEndCard2(new PathEndCard(1,false));
                    gameEngine.setEndCard3(new PathEndCard(1,true));
                    idOfGoldCardImage=gold3.getId();break;
            }
        return idOfGoldCardImage;
        }

        public void determineWinner(View view, String s){
            Log.i("determineWinner", "called: ");
            JSONObject winner = new JSONObject();
            final View viewfinal = view;

            if(!s.isEmpty()){
                try { winner.put("winner",connectedPlayers.get(s).getName());
                      winner.put("role",connectedPlayers.get(s).getRole());
                      winner.put("id",connectedPlayers.get(s).getID());}
                catch (JSONException e) { e.printStackTrace(); }
            }
            else{
                try { winner.put("winner",player.getName());
                      winner.put("role",player.getRole());
                      winner.put("id",player.getID());}
                catch (JSONException e) { e.printStackTrace(); }
            }

            if(gameEngine.lastCardCheck(gameEngine.getCardFromHand(),view,myGrid)){
                socket.emit("gameOver",winner);
                player.setTurn(false);
            }
            else runOnUiThread(new Runnable() {
                @Override
                public void run() { displayFlip(viewfinal); }
            });
            ///moved to game engine
        }

        public void enableActionCardEffects(String type){

            Log.i("enableActionCardEffects", "movable before "+player.getMovable());

            switch (type){
                case "TROLLEY_REPAIR":
                    player.setBrokenTrolley(false); break;
                case "BROKEN_TROLLEY":
                    player.setBrokenTrolley(true); break;
                case "LAMP_REPAIR":
                    player.setBrokenLamp(false); break;
                case "BROKEN_LAMP":
                    player.setBrokenLamp(true); break;
                case "PICK_REPAIR":
                    player.setBrokenPick(false); break;
                case "BROKEN_PICK":
                    player.setBrokenPick(true); break;
                default:
                    throw new IllegalStateException("Unexpected value");
            }

            //the get lamp returns brokenLamp (is true) attribute
            if(player.getLampStatus() || player.getPickStatus() || player.getTrolleyStatus()){
                player.setMovable(false);
            }
            else player.setMovable(true);

            Log.i("enableActionCardEffects", "movable after "+player.getMovable());
        }

        public void renderMessage(String message){
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(this);
            if(gameEngine.getGameCount()==3){
                builder.setTitle("Game Over");
                builder.setMessage(message+" has won the game!").setPositiveButton("Return to Home page", new DialogInterface.OnClickListener() {
                    @Override
                    //start a new game here
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent in  = new Intent(MainActivity.this,MenuActivity.class); //(use finish() to go back to main activity)
                        startActivity(in);
                        finish();
                    }
                });
            }
            else {
                builder.setTitle("The round is over");
                builder.setMessage(message+" has won the round! " +
                        "Start the next round?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            //start a new game here
                            public void onClick(DialogInterface dialogInterface, int i) {
                                gameEngine.setGameCount(gameEngine.getGameCount()+1);
                                resetTheGame();
                                Log.i("YES BUTTON", " called");
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent in  = new Intent(MainActivity.this,MenuActivity.class); //(use finish() to go back to main activity)
                                startActivity(in);
                                finish();
                            }
                        });

            }
            builder.create().show();
        }

        public void resetTheGame(){
            Log.i("resetTheGame", "called!");

            singlePlayerHandler.setBlockMoves(false);

            if(gameEngine.getGameCount()<=3) {
                Log.i("LIKE INSIDE", "called!");

                socket.emit("newGameRequest");
                socket.emit("newGameRequestSingle");

                //reset the possible locations
                possibleLocations = board.fillLocations();
                possibleLocations[myGrid.indexOfChild(start)] = 2;

                addAvailableLocations(start);

                //laid cards
                laidCards = new PathWayCard[40];
                //adding starting card to laid  card
                laidCards[myGrid.indexOfChild(start)] = new PathWayCard(((PathWayCard) deck.getDeck()[0]).getId(), ((PathWayCard) deck.getDeck()[0]).getType(), ((PathWayCard) deck.getDeck()[0]).getImg());

                for (int i = 0; i < laidCards.length; i++) {
                    Log.i("The laid cards", "reset: " + laidCards[i]);
                }

                //hand?
                player.setHand(new ArrayList<Card>());
                player.setMovable(true);
                player.setTurn(true);

                //the round count
                round = (TextView) findViewById(R.id.round);
                runOnUiThread(new Runnable() {@Override public void run() { round.setText(String.valueOf(gameEngine.getGameCount())); } });

                //the grid
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        for (int i = 0; i < myGrid.getChildCount(); i++) {
                            if (!(i == 0 || i == 2 || i == 4 || i == 37)) {
                                myGrid.getChildAt(i).setAlpha(0f);
                            }
                        }
                    }
                });

                ///the gold(rotate if was flipped)
                if(gameEngine.getEndCard1().getIsFlipped()){ gold1.setImageResource(R.drawable.gold_back); }
                if(gameEngine.getEndCard2().getIsFlipped()){ gold2.setImageResource(R.drawable.gold_back); }
                if(gameEngine.getEndCard3().getIsFlipped()){ gold3.setImageResource(R.drawable.gold_back); }

                card1.setAlpha(1f);card2.setAlpha(1f);card3.setAlpha(1f);card4.setAlpha(1f);card5.setAlpha(1f);card6.setAlpha(1f);

            }
        }

        public void customToast(String s,int i){

            LayoutInflater inflater = getLayoutInflater();
            if(i==0){
                View layout = inflater.inflate(R.layout.custom_toast_regular, (ViewGroup) findViewById(R.id.custom_toast_container));
                TextView tv = (TextView) layout.findViewById(R.id.txtvw);
                tv.setText(s);
                Toast toast = new Toast(getApplicationContext());
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();
            }
            else if(i==1){
                View layout = inflater.inflate(R.layout.custom_toast_alert, (ViewGroup) findViewById(R.id.custom_toast_alert));
                TextView tv = (TextView) layout.findViewById(R.id.txtvwalert);
                tv.setText(s);
                Toast toast = new Toast(getApplicationContext());
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();
            }
        }

        public void setAttack(String id, String card){

            Player p;
            p = fetchPlayerWithId(id);
            Log.i("setAttack", " for "+p.getName());
            switch (card){
                case "TROLLEY_REPAIR":
                    p.setBrokenTrolley(false); break;
                case "BROKEN_TROLLEY":
                    p.setBrokenTrolley(true); break;
                case "LAMP_REPAIR":
                    p.setBrokenLamp(false); break;
                case "BROKEN_LAMP":
                    p.setBrokenLamp(true); break;
                case "PICK_REPAIR":
                    p.setBrokenPick(false); break;
                case "BROKEN_PICK":
                    p.setBrokenPick(true); break;
                default:
                    throw new IllegalStateException("Unexpected value");
            }
            if(p.getLampStatus() || p.getTrolleyStatus() || p.getPickStatus()){
                p.setMovable(false);
            }
            else p.setMovable(true);

        }

        public Player fetchPlayerWithId(String s){

            for(Player p: connectedPlayers.values()){
                if(p.getID().equals(s)){
                    return p;
                }
            }
            return null;
        }

        public boolean checkEmptyHand(){
            int j=0; boolean empty=true;
            while(j<player.getHand().size() && empty){
                if(player.getHand().get(j) instanceof PathWayCard || player.getHand().get(j) instanceof ActionCards){
                    empty=false;
                }
                j++;
            }

            if(empty){
                Log.i("EMPTY","cheese");
                socket.emit("emptyHand");
            }
            return empty;
        }

        @Override
        protected void onDestroy() {
            super.onDestroy();
            socket.disconnect();
        }

}