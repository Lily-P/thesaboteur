package com.lala.thesaboteur;

public class ActionCards extends Card {

    /** The specific type of the action */
    private ActionCardType type;

    /**Image resource identifier*/
    private int img;

    /**
     * Constructor for the abstract Card class
     *
     * @param id the unique identifier for a card
     */
    public ActionCards(int id, ActionCardType a, int i) {
        super(id);
        this.type=a;
        this.img=i;
    }

    public enum ActionCardType {
        TROLLEY_REPAIR(actionType.HEAL),
        BROKEN_TROLLEY(actionType.DAMAGE),
        LAMP_REPAIR(actionType.HEAL),
        BROKEN_LAMP(actionType.DAMAGE),
        PICK_REPAIR(actionType.HEAL),
        BROKEN_PICK(actionType.DAMAGE),
        MAP(actionType.PEEK);

        actionType action;

        ActionCardType(actionType a) {
            this.action = a;
        }
    }

    public enum actionType {
        HEAL(0), DAMAGE(-1),PEEK(1);
        private final int val;

        actionType(int val) { this.val = val; }
    }

    @Override
    public CARD_TYPE type() {
        return CARD_TYPE.ACTION;
    }

    public ActionCardType getType(){
        return type;
    }

    @Override
    public int getImg() {return img;}

}
