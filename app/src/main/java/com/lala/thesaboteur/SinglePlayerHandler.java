 package com.lala.thesaboteur;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class SinglePlayerHandler {

    public int[] deck= new int[58];
    private boolean blockMoves = false;

    public SinglePlayerHandler(){
    }

    private void initializeHand(Player p){
        //p.setHand();
    }

    public ArrayList<Integer> moveHandler(int[] locations){
        //the the first card from hand // use alg to see if it can be laid or played // display the move // refill hand // send the request back
        ArrayList chosenLocs = new ArrayList();

        for(int i=0;i<locations.length;i++){
            if(locations[i]==1){
                Log.i("LOCATION", ": "+i);
               chosenLocs.add(i);
            }
        }
     return chosenLocs;
    }

    public void initializeHands(HashMap<String,Player> cPlayers, JSONArray hands, JSONArray ids, Deck deck) throws JSONException {

        for(int i=0;i<ids.length();i++){ //ids( 1, 2)
            //if there is such key then set the appropriate hand to player in connected players
            if(cPlayers.containsKey(ids.get(i).toString())){

                for(int j=0;j < hands.getJSONArray(i).length();j++){
                   int cardNo = hands.getJSONArray(i).getInt(j);
                    cPlayers.get(ids.get(i).toString()).getHand().add(deck.getDeck()[cardNo]);
                    Log.i("initializeHands", "ids "+deck.getDeck()[cardNo].getId());
                }

            }
        }
    }

    public int[] getDeck() { return deck; }
    public void setDeck(int[] deck) { this.deck = deck; }

    public void setBlockMoves(boolean terminateMoves) { this.blockMoves = terminateMoves; }
    public boolean getBlockMoves() { return blockMoves; }
}
