package com.lala.thesaboteur;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

public class NicknameActivity extends AppCompatActivity {

    //declare socket
    private String mode;

    private Button btn;

    private EditText nickname,roomName;
    public static final String NICKNAME = "usernickname", ROOMNAME="roomname";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nickname);

        btn = (Button)findViewById(R.id.joinButton) ;
        nickname = (EditText) findViewById(R.id.nickname);
        roomName = (EditText) findViewById(R.id.room);

        mode = getIntent().getExtras().getString(MenuActivity.MULTIPLAYER);
        if(mode.equals("create")){
            btn.setText("Create");
        }
        else {
            btn.setText("Join");
            roomName.setHint("room ID");
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((!nickname.getText().toString().isEmpty()) && (!roomName.getText().toString().isEmpty())){
                    Intent in  = new Intent(NicknameActivity.this,MainActivity.class);
                    in.putExtra(NICKNAME,nickname.getText().toString());
                    in.putExtra(ROOMNAME,roomName.getText().toString());
                    startActivity(in);
                }
            }
        });
    }
}