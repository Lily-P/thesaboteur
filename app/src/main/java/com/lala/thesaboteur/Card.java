package com.lala.thesaboteur;

public abstract class Card {
    public enum CARD_TYPE {PATHWAY, ACTION, ENDPATH}
    /** The unique identifier of the card */
    private final int id;

    /** The image resource identifier of the card */
    private int img;

    /**
     * Constructor for the abstract Card class
     *
     * @param id the unique identifier for a card
     */
    public Card(int id) {
        this.id = id;
    }

    public abstract CARD_TYPE type();
    public abstract int getImg ();

    public int getId(){return id;}

    @Override
    public String toString() {
        return "Card{" + type() + '}';
    }

}
