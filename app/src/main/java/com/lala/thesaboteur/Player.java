package com.lala.thesaboteur;

import java.util.ArrayList;

public class Player {

    //attributes
    private boolean isTurn=true;
    private String ID;
    private String nickname;
    private int role;
    private int points=0;

    private ArrayList<Card> hand;
    //action cards states
    private boolean brokenLamp=false,brokenTrolley=false, brokenPick =false,peek=false,canMove=true;

    public Player(String id, String nickname){
        this.ID=id;
        this.nickname = nickname;
        hand = new ArrayList<>();
    }

    //getters and setters
    public String getID(){ return ID;}
    public Boolean getTurn(){ return isTurn;}
    public Boolean getLampStatus(){ return brokenLamp;}
    public Boolean getTrolleyStatus(){ return brokenTrolley;}
    public Boolean getPickStatus(){ return brokenPick;}
    public Boolean getPeekStatus(){ return peek;}
    public ArrayList<Card> getHand(){ return hand;}
    public int geSizeOfHand(){ return hand.size();}
    public int getRole(){ return role;}
    public String getName() {return nickname;}
    public Boolean getMovable(){return canMove;}
    public int getPoints() { return points; }
    public boolean emptyHand(){ if(hand.size()==0) return true; else return false;}

    public void setHand(ArrayList<Card> h){this.hand = h;}
    public void setRole(int role){ this.role = role;}
    public void setTurn (boolean b) {this.isTurn = b;}
    public void setPeekStatus(boolean peek){ this.peek=peek;}
    public void setBrokenLamp(boolean b){this.brokenLamp=b;}
    public void setBrokenTrolley(boolean b){this.brokenTrolley=b;}
    public void setBrokenPick(boolean b){this.brokenPick =b;}
    public void setMovable(boolean b){this.canMove=b;}
    public void setPoints(int p){this.points=p;}


}
