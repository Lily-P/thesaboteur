const {join, nextPlayer, fetchUsers, turnHandler, getRandomCards, userLeaves, idGenerator, shuffle} = require('../index.js');


///testing join function 
test('The return value should be an object with 4 attributes specified', ()=>{
	return expect(join('MyId', 'Lala', 'room1', 0)).toEqual({id:'MyId', username:'Lala', roomName: 'room1', role:0});
});

test('The return value should be an object with 4 attributes', ()=>{
	return expect(join('MyId', 'Lala', 'room1', 0)).not.toEqual({id:'MyId2', username:'John', roomName: 'room1'});
});



//testing the next function
test('Should return next player in the array starting from 0th player that has same roomName and different id, if empty returns user itself', ()=>{
	var clients = [];
	for(var i=0;i<8;i++){
		const user = {id:'id'+i, username:'user'+i, roomName: 'room1', role:0}
		clients.push(user)
	}

	const checkUser = {id:'id2', username:'user2', roomName: 'room2', role:0}

	return expect(nextPlayer(clients,checkUser)).toBeUndefined();
});

test('Should return next player in the array starting from 0th player that has same roomName and different id, if empty returns user itself', ()=>{
	var clients = [];

	const checkUser = {id:'id2', username:'user2', roomName: 'room2', role:0}

	return expect(nextPlayer(clients,checkUser)).toEqual({id:'id2', username:'user2', roomName: 'room2', role:0});
});

test('Should return next player in the array starting from 0th player that has same roomName and different id, i.e 1st player', ()=>{
	var clients = [];
	for(var i=0;i<8;i++){
		const user = {id:'id'+i, username:'user'+i, roomName: 'room1', role:0}
		clients.push(user)
	}

	const checkUser = {id:'id0', username:'user0', roomName: 'room1', role:0}

	return expect(nextPlayer(clients,checkUser)).toEqual({id:'id1', username:'user1', roomName: 'room1', role:0});
});

test('Should return 2nd player in the list since first player has different room name e roomName', ()=>{
	var clients = [];

	const first = {id:'id0', username:'user0', roomName: 'room2', role:1}
	clients.push(first)

	for(var i=1;i<8;i++){
		const user = {id:'id'+i, username:'user'+i, roomName: 'room1', role:0}
		clients.push(user)
	}

	const checkUser = {id:'id3', username:'user3', roomName: 'room1', role:0}

	return expect(nextPlayer(clients,checkUser)).toEqual({id:'id1', username:'user1', roomName: 'room1', role:0});
});



//fetch users function
test('Should contain in an array of users the id of player that has same room name as the specified user', ()=>{
	var clients = [];
	const userInSameRoom = {id:'id0', username:'user0', roomName: 'room1', role:1}
	clients.push(userInSameRoom)

	for(var i=1;i<4;i++){
		const user = {id:'id'+i, username:'user'+i, roomName: 'room2', role:0}
		clients.push(user)
	}

	const checkUser = {id:'id1', username:'user1', roomName: 'room1', role:0}

	return expect(fetchUsers(checkUser,clients)).toContain('id0');
});


test('Should return an array of users in the same room as the specified user', ()=>{
	var clients = [];

	const wrongRoom = {id:'id0', username:'user0', roomName: 'room2', role:1}

	for(var i=1;i<4;i++){
		const user = {id:'id'+i, username:'user'+i, roomName: 'room1', role:0}
		clients.push(user)
	}

	const checkUser = {id:'id1', username:'user1', roomName: 'room1', role:0}

	return expect(fetchUsers(checkUser,clients)).toEqual(['id1','id2','id3']);
});

test('Should not contain the id of player that has different room name from the specified user\'s room name', ()=>{
	var clients = [];

	const userInDifferentRoom = {id:'id0', username:'user0', roomName: 'room2', role:1}
	clients.push(userInDifferentRoom)

	for(var i=1;i<4;i++){
		const user = {id:'id'+i, username:'user'+i, roomName: 'room1', role:0}
		clients.push(user)
	}

	const checkUser = {id:'id1', username:'user1', roomName: 'room1', role:0}

	return expect(fetchUsers(checkUser,clients)).not.toContain('id0');
});

test('Should return an empty array if the clients array is empty', ()=>{
	var clients = [];

	const checkUser = {id:'id1', username:'user1', roomName: 'room1', role:0}

	return expect(fetchUsers(checkUser,clients)).toEqual([]);
});



//////testing turns handler function
test('Should return the player who\'s turn it is', ()=>{
	var clients = [];

	for(var i=0;i<4;i++){
		const user = {id:'id'+i, username:'user'+i, roomName: 'room1', role:0}
		clients.push(user)
	}

	return expect(turnHandler(clients)).toEqual({id:'id0', username:'user0', roomName: 'room1', role:0});
});


test('Should return the player who\'s turn it is, in this case should return second player', ()=>{
	var clients = [];

	for(var i=0;i<4;i++){
		const user = {id:'id'+i, username:'user'+i, roomName: 'room1', role:0}
		clients.push(user)
	}

	turnHandler(clients)
	return expect(turnHandler(clients)).toEqual({id:'id1', username:'user1', roomName: 'room1', role:0});
});


test('Should return empty string if there are no players in the array', ()=>{
	var clients = [];
	return expect(turnHandler(clients)).toBeFalsy();
});



////getRandomCards
test('Should return random 6 cards', ()=>{
	var deck = [];

	for(var i=0;i<20;i++){
		deck.push(i)
	}

	return expect(getRandomCards(deck)).toBeTruthy();
});


test('Should return array with undefined if the deck is empty', ()=>{
	var deck = [];

	return expect(getRandomCards(deck)).toEqual([undefined, undefined, undefined, undefined, undefined, undefined]);
});



///userLeaves function testing
test('Should return falsy value since the users array is not specified', ()=>{

	const checkUser = {id:'id2', username:'user2', roomName: 'room1', role:0}
	return expect(userLeaves(checkUser)).toBeFalsy();
});



////id generator function testing    idGenerator
test('Should return a String with length of 6', ()=>{
	return expect(idGenerator(6).length).toBe(6);
});


test('Should return a String with length of 0', ()=>{
	return expect(idGenerator(0).length).toBeFalsy();
});


///suffle array function 
test('Should suffle the array and return new one', ()=>{
	var array=[];

	for(var i=0;i<20;i++){
		array.push(i)
	}

	return expect(shuffle(array)[0]).not.toBe(0);
});


test('Should suffle the array and return new one, if empty returns empty array', ()=>{
	var array=[];

	return expect(shuffle(array)).toEqual([]);
});

































