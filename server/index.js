const express = require('express'),
http = require('http'),
app = express(),
server = http.createServer(app),
io = require('socket.io').listen(server);

module.exports = server;

server.listen(3000,()=>{
	console.log('Node app is running on port 3000')
}); 

app.get('/', (req, res) => {
	res.send('Game Server is running on port 3000')
});

//the array of connected players
var users = [];

//hashmap for roomname and deck pair
let decksPerRoom = new Map();

//role assigning
var saboteur = [1,0,0];

//hash for array of players and rooms
let clientsPerRoom = new Map();

//points
let pointsMap = new Map();

////move this to each room
let emptyHandCnt=0;



//listen to connect event
io.on('connection', (socket) => {
	console.log('user connected')

	//"my" cards
	let chosenCards = [];
	emptyHandCnt=0;


	//choose role for player and delete that role
	console.log('my role '+ saboteur[0]);
	let role = saboteur[0];
	saboteur.push(saboteur.shift());

	//listen to an emitted event called 'join'
	socket.on('join', function(userNickname,roomName) {
	//check if there are players in room-> if not the roomname is soccket.id-> if there are find the room and join id
		if(users.length==0){
			roomName = socket.id;
		}

		let gameCount=1;

		const user = userJoin(socket.id, userNickname, roomName, role);
		console.log("user name is: "+ user.username+ " id is "+ user.id+" room name is "+user.roomName);
	        
	    //join room
	    socket.join(user.roomName);
	    socket.emit('joinedRoom',{r: user.roomName});

	    console.log(userNickname +" : has joined the game !");

	    ///room size check
	    let roomSize = io.sockets.adapter.rooms[user.roomName];
		console.log("this is the room size "+roomSize.length);
		if(roomSize.length<3){
			io.in(user.roomName).emit('waitingForPlayers');
		}
		else if(roomSize.length==3){
			io.in(user.roomName).emit('canPlay');

		}

	    //set points map to 0
	    pointsMap.set(user.id,0);

	    //check if there is existing deck for this room name 
		if(!decksPerRoom.has(roomName)){
			let deckOfCards = [];
			fillArray(deckOfCards);

			decksPerRoom.set(roomName, deckOfCards);
		}
			

		//choose 6 random numbers
		chosenCards = choose6(decksPerRoom.get(roomName));
		console.log('chosen Cards ' + chosenCards);
	    
	    //send "me" the id of my socket
		socket.emit('socketID', { id: socket.id });

		//send everyon else "my" id
		socket.broadcast.to(user.roomName).emit('newPlayer', {id: user.id, username: user.username });

		//send "me" currently connected players (modify to only send the players in the same room) 
		if(users.length>=1){
			socket.emit('getPlayers',users);
		}

		//choose gold card depending on the room name
		let gold = user.roomName.charCodeAt(0)%3;

		//send 6 chosen cards to "me" and my role and gold card
		console.log("gold "+gold);
		socket.emit('dealCards',{hand: chosenCards, role: role, gold: gold});


		//handling turns
			let tmp = fetchUsers(user,users);
			console.log('FETCHED '+tmp);

			clientsPerRoom.set(user.roomName, tmp);


			let nextDude = turnsHandler(clientsPerRoom.get(user.roomName));
			io.in(user.roomName).emit('turnOf',{turn: nextDude});

			socket.on('changeTurn',(data)=>{
				let newDude = turnsHandler(clientsPerRoom.get(user.roomName));
				console.log('new next dude '+newDude);
				io.in(user.roomName).emit('turnOf',{turn: newDude});
			});



		//get the moved card data
		socket.on('cardMoved',(data)=>{
    		let d = {
	    		movedCard: data.movedCardId,
	    		movedPlace: data.movedPlaceId,
	    		possibleLocations: data.updatedPositions,
	    		movedPlaceGrid: data.movedPlaceGrid
    		};

    		console.log("info about moved card "+d.movedCard+" : " +d.movedPlace+" : "+d.possibleLocations[0]);
    		socket.broadcast.to(user.roomName).emit('playersMove', d);
    	});

    	//action card used
    	socket.on('actionUsed',(data)=>{
    		let dd= {
    			usedCard: data.usedCard,
    			usedAgainst: data.usedAgainst
    		}; 			

    		console.log("info about action card used "+dd.usedCard+" : " +dd.usedAgainst);
    		io.in(user.roomName).emit("attacked", dd);

    	});

		//refill request
    	socket.on('requestNewCard', function() {
	    	if(decksPerRoom.get(roomName).length==0){
	    		console.log('deck ran out');
	    		io.in(user.roomName).emit('deckRanOut');
	    	}
		    //refill hand
		    refill(decksPerRoom.get(user.roomName), socket);
		});

    	///broadcast that game is over
		socket.on('gameOver',(data)=>{
			let winner = {
				win: data.winner,
				winRole: data.role,
				id: data.id};

///////move this to function later
			if(winner.id!=""){
				for(var i=0;i<users.length;i++){
					if(winner.winRole==0){
						if(users[i].role==0 && users[i].id!=winner.id){
							pointsMap.set(users[i].id,pointsMap.get(users[i].id)+1);
						}
						else if(users[i].id==winner.id){
							pointsMap.set(users[i].id,pointsMap.get(users[i].id)+2);
						}
					}
					else if(winner.winRole==1){
						if(users[i].role==1){
							pointsMap.set(users[i].id, pointsMap.get(users[i].id)+3);
						}
					}
				}
			}
			else{
				for(var i=0;i<users.length;i++){
					if(users[i].role==1){
						pointsMap.set(users[i].id, pointsMap.get(users[i].id)+3);
						winner.win = users[i].username;
						winner.id = users[i].id
					}
				}

			}

			let pointsArray = Array.from( pointsMap.values());
			for(var i=0;i<pointsArray.length;i++){console.log('points '+ pointsArray[i]);}

			if(gameCount==3){
				for(var i=0;i<users.length;i++){
					if(fetchWinner(pointsMap)==users[i].id)
						winner.win = users[i].username;
				}
    			console.log('gameCount '+ gameCount);
    		}

			console.log('winner '+ data.winner);
			io.in(user.roomName).emit('gameOverGuys', {winner: winner.win, points: pointsArray, ids: Array.from(pointsMap.keys())});
    	});


    	socket.on('newGameRequest',(data)=>{
    		console.log('newGameRequest called');
    		gameCount=gameCount+1;
    		emptyHandCnt=0;

    		let deckOfCardsNew = [];
			fillArray(deckOfCardsNew);

			decksPerRoom.set(roomName, deckOfCardsNew);

			let role2 = saboteur[0];
			saboteur.push(saboteur.shift());

    		//new hand for each player in the room
    		chosenCards = choose6(decksPerRoom.get(roomName));
    		socket.emit('dealCards',{hand: chosenCards, role: role2, gold: gold});

    	});

    	socket.on('goldFlipped',(data)=>{
    		console.log('goldFlipped called '+data.flipped);
    		socket.to(user.roomName).emit('flippedCard',{flipped: data.flipped});
    	});


    	socket.on('emptyHand', function() {
    		emptyHandCnt=emptyHandCnt+1;
    		console.log('emptyHand called');
    		let saboteur;
    		let sabId;

    		if(emptyHandCnt==3){
    			console.log('CNT 3');

    			for(var i=0;i<users.length;i++){
					if(users[i].role==1){
						pointsMap.set(users[i].id,pointsMap.get(users[i].id)+3);
						saboteur = users[i].username; 
						sabId = users[i].id;
					}
				}

	    		let winner = {
					win: saboteur,
					winRole: 1,
					id: sabId};

    			io.in(user.roomName).emit('gameOverGuys', {winner: winner.win, points: Array.from(pointsMap.values()), ids: Array.from(pointsMap.keys())});
    		}
    		else if(emptyHandCnt<3){
    			let nextDude = turnsHandler(clientsPerRoom.get(user.roomName));
				io.in(user.roomName).emit('turnOf',{turn: nextDude});
    		}

		});

    });


	socket.on('single',function(userNickname){
    	console.log("single player mode request")

    	let singleDeck = [];
    	let decksOfOpponents = new Map();
    	let gameCount=1;

    	//add user(id,name,room) obj to array
		const user = userJoin(socket.id, userNickname, socket.id, role);
		console.log("(single) user name is: "+ user.username+ " id is "+ user.id+" room name is "+user.roomName);

    	//fill the deck
		fillArray(singleDeck);

		//choose 6 cards and send the player "the hand" role and gold
	    //chosenCards = first6(singleDeck);
	    chosenCards = pick();
	    console.log('(single) chosen cards '+ chosenCards);

		let mockPlayers = initializeMockPlayers(user,decksOfOpponents,singleDeck);
    	console.log(mockPlayers);
    	console.log('(single) size of the deck '+ singleDeck.length);
    	shuffle(singleDeck);


	    //send "me" the id of my socket
		socket.emit('socketID', { id: socket.id });
		//io.in(user.roomName).emit('socketID',{ id: socket.id });

	    //send "me" currently connected players (mock players) 
		if(mockPlayers.length>=1){socket.emit('getPlayers',mockPlayers);}

		//choose gold card depending on the room name
		let gold = user.roomName.charCodeAt(0)%3;

		//send 6 chosen cards to "me" and my role and gold card
		console.log("(single)gold "+gold);
		socket.emit('dealCards',{hand: chosenCards, role: role, gold: gold});

		//send me the opponents' hands
		var rolesOfOp = [];
		rolesOfOp.push(mockPlayers[1].role);
		rolesOfOp.push(mockPlayers[2].role);

		socket.emit('opponentsHand', {opId: Array.from( decksOfOpponents.keys()), opHand: Array.from( decksOfOpponents.values()), opRole: rolesOfOp});

		//refill hand
    	socket.on('requestNewCard', function() {
		    refill(singleDeck, socket);
		});

		//handling turns (me,x,y) (x,me,y)
			let nextDude = turnsHandler(mockPlayers);
			console.log('(single)next dude id '+nextDude.id);
			if (socket.id==nextDude.id){
				socket.emit('turnOf',{turn: nextDude.id});
			}
			else {
				console.log('(single)move handler');
				//triggering event that makes moves
				socket.emit('opponentMove',{turn: nextDude.id});
				//make move here and change the turn
				nextDude = turnsHandler(mockPlayers);
				if (socket.id==nextDude.id){
					socket.emit('turnOf',{turn: nextDude.id});
				}
				else {
					console.log('(single)move handler');
					//make move 
					socket.emit('opponentMove',{turn: nextDude.id});
					//change turn
					nextDude = turnsHandler(mockPlayers);
					socket.emit('turnOf',{turn: nextDude.id});
				}
			}
		

			socket.on('changeTurn',(data)=>{
				let newDude = turnsHandler(mockPlayers); //x
				console.log('(single)NEW next dude id '+newDude.id);

				if (socket.id==newDude.id){
					socket.emit('turnOf',{turn: newDude.id});
				}
				else {
					console.log('(single)move handler');
					//make move
					socket.emit('opponentMove',{turn: newDude.id});
					//change move
					newDude = turnsHandler(mockPlayers); //y
					if (socket.id==newDude.id){
						socket.emit('turnOf',{turn: newDude.id});
					}
					else {
						console.log('(single)move handler');
						//make move here and change the turn
						socket.emit('opponentMove',{turn: newDude.id});

						newDude = turnsHandler(mockPlayers); //me
						console.log('(single)me? '+newDude.id);

						socket.emit('turnOf',{turn: newDude.id});
					}
				}

			});

		//end game //rewrite
		socket.on('gameOver',(data)=>{
		console.log('(single) gameOver called')
		let winner = {
			win: data.winner,
			winRole: data.role,
			id: data.id};

			if(winner.id!=""){
				for(var i=0;i<mockPlayers.length;i++){
					if(winner.winRole==0){
						if(mockPlayers[i].role==0 && mockPlayers[i].id!=winner.id){
							pointsMap.set(mockPlayers[i].id,pointsMap.get(users[i].id)+1);
						}
						else if(mockPlayers[i].role==0 && mockPlayers[i].id==winner.id){
							pointsMap.set(mockPlayers[i].id,pointsMap.get(mockPlayers[i].id)+2);
						}
					}
					else if(winner.winRole==1){
						if(mockPlayers[i].role==1){
							pointsMap.set(mockPlayers[i].id, pointsMap.get(mockPlayers[i].id)+3);
						}
					}
				}
			}
			else{
				for(var i=0;i<mockPlayers.length;i++){
					if(mockPlayers[i].role==1){
						pointsMap.set(mockPlayers[i].id, pointsMap.get(mockPlayers[i].id)+3);
						winner.win = mockPlayers[i].username;
						winner.id = mockPlayers[i].id
					}
				}

			}

			if(gameCount==3){
				for(var i=0;i<mockPlayers.length;i++){
					if(fetchWinner(pointsMap)==mockPlayers[i].id)
					winner.win = mockPlayers[i].username;
				}
    			console.log('gameCount '+ winner.win);
    		}


			let pointsArray = Array.from( pointsMap.values() );
			for(var i=0;i<pointsArray.length;i++){
				console.log('points '+ pointsArray[i]);
			}

			socket.emit('gameOverGuys', {winner: winner.win, points: pointsArray, ids: Array.from(pointsMap.keys())}); 
    	});


    	socket.on('newGameRequestSingle',(data)=>{
    		console.log('(single)newGameRequest called');
    		gameCount=gameCount+1;
    		singleDeck = [];
			fillArray(singleDeck);

    		chosenCards = choose6(singleDeck);
    		socket.emit('dealCards',{hand: chosenCards, role: role, gold: gold});

    	});


    });



    //handle disconnect
	socket.on('disconnect', function() {
		const user = userLeaves(socket.id);

	    //send everyone but itself the id of disconnected user
	    console.log( 'user has left the room '+user.roomName);
	    io.to(user.roomName).emit('playerDisconnected', {id: socket.id });
	});

});


function fillArray(deck){
	for(var i=0; i<64;i++){
		deck.push(i);
	}
}

function clearArray(arr){
	return arr.length = 0;
}

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
    let j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

// Join user to game
function userJoin(id, username, roomName, role) {
  const user = { id, username, roomName, role };

  users.push(user);

  return user;
}

// User leaves the game
function userLeaves(id) {
//find the user with given id and take that user out of array
  const index = users.findIndex(user => user.id === id);
  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
}

function choose6(arr){
	//choose 6 random numbers
	let chosenC = [];
	for (let i = 0; i < 6; i++) {
		let cardNo = arr[0];
		chosenC.push(cardNo); 
		arr.shift();	
	shuffle(arr);
	}
	return chosenC;
}


//handle the turns
function turnsHandler(playersInRoom){
	let result='';
	if(playersInRoom.length>0){
		result = playersInRoom[0];
		playersInRoom.push(playersInRoom[0]);
		playersInRoom.shift();

	}
	return result;
}

//refill function
function refill(deck, socket){
    //refill hand
    if(deck.length>0){
    	console.log("refill card "+deck[0]);
    	socket.emit('refill',{refillCard: deck[0]});
    	//delete sent card
    	deck.shift();	
    	console.log("length after refill "+deck.length);
    }
    else {
    	socket.emit('refill',{refillCard: -1});
    }
	
}

function next(clients,u){
	if(clients.length>1){
		console.log("the length of tmp "+clients.length);
		for(i=0;i<clients.length;i++){
			if(u.roomName==clients[i].roomName && u.id!=clients[i].id){
				return clients[i];
			}
		}
	}
	else return u;
}


function fetchUsers(u,users){
	let sameroom=[];
	for(i=0;i<users.length;i++){
		if(u.roomName==users[i].roomName){
			sameroom.push(users[i].id);
		}
	}

	return sameroom;
}

function initializeMockPlayers(user,myMap,deck){
	console.log('inside the initializeMockPlayers method')

	let singlePlayerArray = [];

	singlePlayerArray.push(user);

	let mockRoom = user.roomName;
	let opponent1 = userJoin(randomString(8), randomString(5), mockRoom, saboteur[0]);
	let opponent2 = userJoin(randomString(8), randomString(5), mockRoom, saboteur[1]);

	singlePlayerArray.push(opponent1);
	singlePlayerArray.push(opponent2);

	myMap.set(opponent1.id, first6(deck));
	myMap.set(opponent2.id, first6(deck));

	pointsMap.set(user.id,0);
	pointsMap.set(opponent1.id,0);
	pointsMap.set(opponent2.id,0);

	console.log('opponent1 '+ myMap.get(opponent1.id));
	console.log('opponent2 '+ myMap.get(opponent2.id));

  return singlePlayerArray;
}

function first6(deck){
	var chosen = []
	let k=0;
	for(var i=0;i<6;i++){
		let cardNo = deck[k];
		chosen.push(cardNo); 
		deck.push(deck.shift());
		k=k+2;	
	}

	return chosen;
}

function pick(){
	let arr = [];
	while(arr.length < 6){
	    var r = Math.floor(Math.random() * 63) + 1;
	    if(arr.indexOf(r) === -1) arr.push(r);
	}
	console.log(arr);
	return arr;
}



function randomString(length){

   var result = '';
   var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

function fetchWinner(pointsMap){
	var max = 0;
	var result;
	for(let [key, value] of pointsMap.entries()){
		if(value>max){
			max = value;
			result = key;
		}
	}
 	return result;
}

module.exports = {
    join: userJoin,
    nextPlayer: next,
    fetchUsers: fetchUsers,
    turnHandler: turnsHandler,
    getRandomCards: choose6,
    userLeaves: userLeaves,
    idGenerator: randomString,
    shuffle: shuffle
};













